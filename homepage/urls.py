from django.conf.urls import url
from django.contrib import admin
from django.urls import path, register_converter
from django.conf.urls.static import static
from .views import *


urlpatterns = [
    path('', indexMain, name='indexMain'),
    path('transaction/', indexTransaction, name='indexTransaction'),
    path('kritik/', kritik, name="kritik"),
    path('getKritik/', getKritik, name="getKritik"),
    path('signup/', signUp, name="signup"),
    path('login/', loginUser, name="loginUser"),
    path('logout/', logoutUser, name="logoutUser"),
    path('apd/', apd, name="apd"),
    path('handsanitizer/', handsanitizer, name="handsanitizer"),
    path('masker/', masker, name="masker"),
    path('product/<str:pk>', indexProduct, name='indexProduct'),
    path('review/<str:pk>', indexReview, name='indexReview'),
    path('postReview/<str:pk>', postReview, name='postReview'),
    path('cariusername/', cariNamaAkun, name="cariNamaAkun"),
    path('searchusername/', searchUsername),
    # path('category/', category, name="category"),
   
]