const form = document.getElementById("form");
console.log(form)
const customer = document.getElementById("id_customer")
const noHP = document.getElementById("id_noHP")
const alamat = document.getElementById("id_alamat")
const orderId = document.getElementById("id_orderID")
const produk = document.getElementById("id_produk")
const csrf = document.getElementsByName("csrfmiddlewaretoken")
console.log(csrf)

form.addEventListener('submit', e=>{
    e.preventDefault()

    const fd = new FormData()
    fd.append('csrfmiddlewaretoken', csrf[0].value)
    fd.append('customer', customer.value)
    fd.append('noHP', noHP.value)
    fd.append('alamat', alamat.value)

    
    $.ajax({
        type: 'POST',
        url: '{% url "indexTransaction" %}',
        enctype: 'multipart/form-data',
        data: fd,   
        success: function(response){ //jika sukses 
            console.log(response) //response di console
            
            // buat alert
            const suksesteks = 'successfully saved '+ response.nama
            handleAlerts('success', suksesteks)
           
            $("#jumlah_feedback").html(response.jumlah);
            
            // reset formnya 
            setTimeout(()=>{
                customer.value = ""
                noHp.value = ""
                alamat.value = ""

            }, 5000)
        },
        //error: function(req, err){ console.log('message : ' + err); },
        error: function(xhr, textStatus, error) {
            console.log('response: ' + xhr.responseText);
            console.log('status: ' + xhr.statusText);
            console.log('status: ' + textStatus);
            console.log(error);
        },
        /*
        error: function(error){ // jika error 
            //console.log(error)
            console.log(JSON.stringify(error))
            handleAlerts('danger', 'ups..something went wrong')
        },*/
        cache: false,
        contentType: false,
        processData: false,
    })
})