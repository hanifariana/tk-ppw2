$(function() {
    $('#input-name').keyup(function() {
        var keyword = $('#input-name').val();
        $.ajax({
            method: 'GET',
            url: '/getKritik/',
            dataType: 'json',
            success: function(data) {
                $('#message').empty();
                var html = '';
                console.log("test")
                if (data.length === 0) {
                    html += '<h3 class="txt-setting">Error.</h3>';
                    $('#message').append(html);
                } else {
                    for (var i = 0; i < data.length; i++) {
                        if ((data[i]['name']).includes(keyword)) {
                            html += '<div class="card b-green mb-2 w-100 ">';
                            html = html + '<h3>' + data[i]['name'] + '</h3>';
                            html = html + '<p>' + data[i]['message'] + '</p>' + '</div>';
                        }
                    }
                    $('#message').append(html);
                }
                
            }
        })
    })
})
$(document).ready(function() {
    $("#submitBtn").click(function() {
        $.ajax({
            type: "POST",
            url: "/kritik/",
            data: $("#Kritik2Form").serialize(),
            success: function(response) {
                $("#message_input").val("")
                $("#input-name").trigger("keyup");
            }
        });
    });
})