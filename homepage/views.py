from django.shortcuts import render, redirect
from .models import *
from .forms import *
from datetime import datetime, date
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.db.models import Q
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.core import serializers
from django.http.response import HttpResponseRedirect
import json
from django.contrib.auth.decorators import login_required


from django.shortcuts import get_object_or_404

# Create your views here.
def indexMain(request):
    return render(request, 'landingPage.html')


def indexTransaction(request):
    transactions = Transaction.objects.all()

    context = {
        'transactions': transactions,
    }
    return render(request, 'transactionPage.html', context)

# def indexTransaction(request):
#     form = TransactionCreateForm(request.POST)

#     data = {}
    
#     if request.is_ajax():
#         if form.is_valid():
#             form.save()
#             # data['orderId'] = form.cleaned_data.get('orderId')
#             data['customer'] = form.cleaned_data.get('customer')
#             # data['transDate'] = form.cleaned_data.get('transDate')
#             # data['produk'] = form.cleaned_data.get('produk')  
#             data['noHP'] = form.cleaned_data.get('noHP')
#             data['alamat'] = form.cleaned_data.get('alamat')
#             return JsonResponse(data)

#     context = {
#         'form': form,
#     }

#     return render(request, 'transactionPage.html', context)

def signUp(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            messages.success(request, "Successfully created a user with username {}".format(username))
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form' : form})

def loginUser(request): 
    if request.method == 'POST': 
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, 'Logged in as {}'.format(username))
            return redirect('/')
        else:
            messages.error(request, 'Username or password is incorrect')
    return render(request, 'login.html')

def logoutUser(request):
    logout(request)
    messages.success(request, 'Successfully logged out!')
    return redirect('/')


def apd(request):
    return render(request, 'apd.html', {'apd_list': Product.objects.filter(jenis__nama__contains='APD')})

def masker(request):
    return render(request, 'masker.html', {'masker_list': Product.objects.filter(jenis__nama__contains='Masker')})

def handsanitizer(request):
    return render(request, 'handsanitizer.html', {'handsanitizer_list': Product.objects.filter(jenis__nama__contains='Hand Sanitizer')})

def indexProduct(request, pk):
    selectedProduct = Product.objects.get(id=pk)
    orderIdTmp = selectedProduct.nama
    print(orderIdTmp)
    initial_data = {
        'orderId': orderIdTmp,
        'transDate': datetime.now(),
        'produk': selectedProduct,
    }
    form = TransactionCreateForm(request.POST or None, initial=initial_data)
    print(initial_data)
    # reviews = Review.objects.filter(produk=selectedProduct)
    if form.is_valid():
        print('save berhasil')
        form.save()
        selectedProduct.stok -= 1
        selectedProduct.save()
        messages.success(request, 'Produk berhasil dipesan')
    else:
        print(form.errors)
        print("gagal")

    context = {
        'selectedProduct': selectedProduct,
        'form': form,
        # 'reviews': reviews,
    }
    return render(request, 'productPage.html', context)

# def category(request):
#     return render(request,'category.html')

# def cari(request):
#     q = request.GET['q']
#     hasil = []
#     products = Product.objects.all()
#     for i in products:
#         try:
#             price = int(q)
#             if(price>=int(i.harga)):
#                 hasil.append({
#                     'nama':i.nama,
#                     'harga':i.harga,
#                     'jenis':i.jenis,
#                     'stok':i.stok,
#                     'gambar':i.gambar
#                 })
#         except:
#             if(q.lower()==i.nama.lower()):
#                 hasil.append({
#                     'nama':i.nama,
#                     'harga':i.harga,
#                     'jenis':i.jenis,
#                     'stok':i.stok,
#                     'gambar':i.gambar
#                 })
#     data={
#         'hasil':hasil
#     }
#     return JsonResponse(data, safe=False)

# def kritik(request):
#     username = 'anon'
#     if request.user.is_authenticated:
#         username = request.user
#         # print(username)

#     if request.POST:
#         nama = username
#         isi = request.POST.get('isi')
#         Kritik.objects.create(nama=nama, isi=isi)
#         return HttpResponseRedirect('/category/')
#     else:
#         form = KritikForm()
#         kritiks = Kritik.objects.all()

#         for kritik in kritiks:
#             print(kritik)
        
#         context = {
#             'kritiks':kritiks,
#             'form':form
#         }
#         return render(request, 'category.html', context)

# def kritikajax(request):
#     argument = request.GET['q']
#     # argument = request.POST.get('q', none)
#     username = request.user
#     Kritik.objects.create(nama=username, isi=argument)
#     datastr = serializers.serialize("json", Kritik.objects.all())
#     data = json.loads(datastr)

#     return JsonResponse(data, safe=False)

@login_required(login_url="/login")
def kritik(request):
    response = {}
    if request.method == 'POST':
        if request.is_ajax():
            kritik_form = Kritik2Form(request.POST)
            if kritik_form.is_valid():
                kritik = Kritik()
                kritik.name = request.user.username
                kritik.message = kritik_form.cleaned_data['message']
                kritik.save()
                return JsonResponse({'success':True})
        return JsonResponse({'success':False})
    else:
        form = Kritik2Form()
        response = {'form':form}
        response['data'] = Kritik.objects.all()
        return render(request, 'kritik.html',response)


def getKritik(request):
    userReq = Kritik.objects.all().values()
    lst = list(userReq)
    #print(lst)
    return JsonResponse(lst, safe=False)


def indexReview(request, pk):
    user = request.user
    form = ReviewPageCreateForm()
    selectedProduct = get_object_or_404(Product,id=pk)
    data = {
        'produk': selectedProduct
    }
    review = Review.objects.filter(produk=selectedProduct)
    total = 0
    jumlah_bintang_1 = 0
    jumlah_bintang_2 = 0
    jumlah_bintang_3 = 0
    jumlah_bintang_4 = 0
    jumlah_bintang_5 = 0
    for rev in review:
        total += rev.bintang
        if rev.bintang == 1:
            jumlah_bintang_1 += 1
        elif rev.bintang == 2:
            jumlah_bintang_2 += 1
        elif rev.bintang == 3:
            jumlah_bintang_3 += 1
        elif rev.bintang == 4:
            jumlah_bintang_4 += 1
        elif rev.bintang == 5:
            jumlah_bintang_5 += 1
    try:
        avg = total / len(review)
        avg = round(avg, 2)
    except ZeroDivisionError:
        avg = 0
    avg = round(avg, 2)
    context = {
        'selectedProduct': selectedProduct,
        'review': review,
        'average': avg,
        'form' : form,
        'postPK' : pk,
        'user' : user,
        'bintang1': jumlah_bintang_1,
        'bintang2': jumlah_bintang_2,
        'bintang3': jumlah_bintang_3,
        'bintang4': jumlah_bintang_4,
        'bintang5': jumlah_bintang_5

    }
    return render(request, 'reviewPage.html', context)


def postReview(request, pk) :
    selectedProduct = Product.objects.get(id=pk)
    data ={
        'produk' : selectedProduct
    }
    response_data = {}
    if request.method == "POST":
        print(request.POST)
        headline = request.POST.get('headline')
        ulasan = request.POST.get('ulasan')
        bintang = request.POST.get('bintang')

        response_data['headline'] = headline
        response_data['ulasan'] = ulasan
        response_data['bintang'] = bintang
        response_data['tanggal'] = datetime.now().strftime("%m-%d-%Y")

        Review.objects.create(produk = selectedProduct, pengulas = request.user,
        headline = headline, ulasan = ulasan, bintang = bintang)
        return JsonResponse(response_data)

@login_required(login_url="/login")
def cariNamaAkun(request): 
    user = request.user
    return render(request, 'cariUsername.html')

def searchUsername(request):
    usr_name = request.GET['q']
    data = {}
    if usr_name:
        users = User.objects.filter(username__icontains=usr_name)
        if users.exists():
            data = list(users.values())
            return JsonResponse(data, safe=False)

    else:
        users = User.objects.all()
    return JsonResponse(data, safe=False)


