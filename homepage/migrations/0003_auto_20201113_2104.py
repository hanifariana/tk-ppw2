# Generated by Django 3.1.2 on 2020-11-13 14:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0002_auto_20201111_2236'),
    ]

    operations = [
        migrations.CreateModel(
            name='Jenis',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(choices=[('APD', 'APD'), ('Masker', 'Masker'), ('HandSanitizer', 'HandSanitizer')], max_length=120)),
            ],
        ),
        migrations.RemoveField(
            model_name='product',
            name='style',
        ),
        migrations.DeleteModel(
            name='Style',
        ),
        migrations.AddField(
            model_name='product',
            name='jenis',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='homepage.jenis'),
        ),
    ]
