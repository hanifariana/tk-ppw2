# Generated by Django 3.1.1 on 2020-12-31 20:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('homepage', '0004_auto_20201113_2107'),
    ]

    operations = [
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headline', models.CharField(max_length=150)),
                ('ulasan', models.TextField()),
                ('bintang', models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5)], default=5)),
                ('tanggal', models.DateField(auto_now_add=True)),
                ('pengulas', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('produk', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='homepage.product')),
            ],
        ),
    ]
