from django import forms
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.forms import fields
class TransactionCreateForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = '__all__'
        widgets = {
            'orderId': forms.HiddenInput(attrs={
                'class': 'form-control'
            }),
            'customer': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'produk': forms.HiddenInput(attrs={
                'class': 'form-control'
            }),
            'transDate': forms.HiddenInput(attrs={
                'class': 'form-control'
            }),
            'noHP': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'alamat': forms.Textarea(attrs={
                'class': 'form-control'
            }),
        }

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=30, required=False, help_text='Optional.')
    full_name = forms.CharField(max_length=30, required=False, help_text='Optional.')

    class Meta:
        model = User
        fields = ('username', 'full_name','email', 'password1', 'password2')

class KritikForm(ModelForm):
    isi = forms.CharField(widget=forms.Textarea({'class' : 'form-control','maxlength':'100', 'placeholder':'Bye-Covid menurut saya...'}), label='')
    class Meta:
        model = Kritik
        fields = ['isi']

class Kritik2Form(forms.Form):

    message = forms.CharField(label="kritik & saran", widget=forms.Textarea(attrs={
        'rows': 6,
        'style': 'resize:none;',
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'bagus...',
        'required': True,
        'id':'message_input'
    }))


class ReviewPageCreateForm(forms.Form):
    pilihanRate = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )
    headline = forms.CharField(required = False ,widget= forms.TextInput(attrs={
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Plese enter your headline',
    }))
    ulasan = forms.CharField(required = False, widget= forms.Textarea(attrs={
        'class': 'form-control',
        'type': 'text',
        'placeholder': 'Please enter your review',
    }))
    bintang = forms.IntegerField(required=False,widget= forms.Select(choices = pilihanRate,attrs={
        'class': 'form-control',
        'placeholder' : 'Please enter the star',
    }))
