from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from .models import *
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
# from .apps import IndexConfig
from django.http import HttpRequest

from .forms import *



# Create your tests here.
class ByeCovidTest(TestCase):
    # Authentication
    def test_login_page_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_forgot_password_page__url_is_exist(self):
        response = Client().get('/accounts/password/reset/')
        self.assertEqual(response.status_code, 200)

    def test_done_password_page_is_exist(self):
        response = Client().get('/accounts/password/reset/done/')
        self.assertEqual(response.status_code, 200)

    def test_reset_done_page_url_is_exist(self):
        response = Client().get('/accounts/password/reset/key/done/')
        self.assertEqual(response.status_code, 200)

    def test_search_username_page_is_using_right_function(self):
        found = resolve('/cariusername/')
        self.assertEqual(found.func, cariNamaAkun)

    def test_login_page_is_using_login_function(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginUser)

    def test_login_page_is_using_correct_html(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_signup_page_url_is_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_signup_page_is_using_signup_function(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signUp)

    def test_signup_page_is_using_correct_html(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_search_username_page_url_is_exist(self):
    	response = Client().get('/cariusername')
    	self.assertEqual(response.status_code, 301)

    def test_search_username_page_url_is_exist_with_login(self):
    	user = User.objects.create(username='testuser')
    	user.set_password('1234589')
    	user.save()
    	c = Client()
    	logged_in = c.login(username='testuser', password='1234589')
    	response = c.get('/cariusername/')
    	self.assertEqual(response.status_code, 200)

    #landing page
    def test_landing_page_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingPage.html')

    #html
    def test_transaction_page_is_using_correct_html(self):
        response = Client().get('/transaction/')
        self.assertTemplateUsed(response, 'transactionPage.html')

    def test_handsanitizer_page_is_using_correct_html(self):
    	user = User.objects.create(username='testuser')
    	user.set_password('1234589')
    	user.save()
    	c = Client()
    	logged_in = c.login(username='testuser', password='1234589')
    	response = c.get('/handsanitizer/')
    	self.assertTemplateUsed(response, 'handsanitizer.html')

    def test_masker_page_is_using_correct_html(self):
    	user = User.objects.create(username='testuser')
    	user.set_password('1234589')
    	user.save()
    	c = Client()
    	logged_in = c.login(username='testuser', password='1234589')
    	response = c.get('/masker/')
    	self.assertTemplateUsed(response, 'masker.html')


    def test_apd_page_is_using_correct_html(self):
    	user = User.objects.create(username='testuser')
    	user.set_password('1234589')
    	user.save()
    	c = Client()
    	logged_in = c.login(username='testuser', password='1234589')
    	response = c.get('/apd/')
    	self.assertTemplateUsed(response, 'apd.html')

    def test_search_username_is_using_correct_html(self):
    	user = User.objects.create(username='testuser')
    	user.set_password('1234589')
    	user.save()
    	c = Client()
    	logged_in = c.login(username='testuser', password='1234589')
    	response = c.get('/cariusername/')
    	self.assertTemplateUsed(response, 'cariUsername.html')

    def test_forgot_password_page_is_using_correct_html(self):
        response = Client().get('/accounts/password/reset/')
        self.assertTemplateUsed(response, 'account/password_reset.html')

    def test_done_password_page_is_using_correct_html(self):
        response = Client().get('/accounts/password/reset/done/')
        self.assertTemplateUsed(response, 'account/password_reset_done.html')

    def test_reset_done_page_url_is_using_correct_html(self):
        response = Client().get('/accounts/password/reset/key/done/')
        self.assertTemplateUsed(response, 'account/password_reset_from_key_done.html')

    # def test_review_page_is_using_correct_html(self):
    #     response = Client().get('/review/1#formReview/')
    #     self.assertTemplateUsed(response, 'reviewPage.html')
        
    def test_json_url(self):
        response = Client().get('/data/')
        self.assertEqual(response.status_code,404)

    def test_json_username_url(self):
    	response = Client().get('/searchUsername/')
    	self.assertEqual(response.status_code,404)

    #model
    def test_jenis_model(self):
        Jenis.objects.create(nama='APD')
        count = Jenis.objects.all().count()
        self.assertEqual(count, 1)

    def test_product_model(self):
        Jenis.objects.create(nama='APD')
        Product.objects.create(nama='Test', harga=1000000, deskripsi='This is a test',
                               stok=25, jenis=Jenis.objects.get(id=1), gambar='https://i.imgur.com/SkKfJ9J.png')
        count = Product.objects.all().count()
        self.assertEqual(count, 1)

    def test_kritik_model(self):
        Kritik.objects.create(name='hanifa', message='test')
        count = Kritik.objects.all().count()
        self.assertEqual(count,1)

    def test_model_kritik(self):
        kritik = Kritik.objects.create(name="hanifa", message="wow")
        self.assertIn('hanifa', str(kritik))
        kritik_dict = kritik.get_dict()
        self.assertIn('hanifa', kritik_dict['name'])
        self.assertIn('wow', kritik_dict['message'])


    #views
    def test_product_page_is_not_none(self):
        self.assertIsNotNone(indexProduct)

    def test_review_page_is_not_none(self):
        self.assertIsNotNone(indexReview)    
    def test_kritik_view(self):
        response = self.client.post('/kritik/', data={
            'name': 'hanifa',
            'message': 'keren',
        })
        self.assertFalse(Kritik.objects.filter(name="hanifa").exists())

    def test_kritik_view2(self):
        response = self.client.post('/kritik/', data={
            'name': 'hsss',
            'message': 'ssss',
        })
        self.assertFalse(Kritik.objects.filter(message="ssss").exists())

    #url
    def test_product_page_url_is_exist(self):
        Jenis.objects.create(nama='APD')
        Product.objects.create(nama='test', harga=1000000, deskripsi='a test',
                               stok=25, jenis=Jenis.objects.get(id=1), gambar='https://i.imgur.com/SkKfJ9J.png')
        response = Client().get('/product/1')
        self.assertEqual(response.status_code, 200)


    #form
    def test_form_kritik(self):
        kritik_form = Kritik2Form(data={'message':'test'})
        self.assertTrue(kritik_form.is_valid())
        self.assertEqual(kritik_form.cleaned_data['message'], "test")

    def test_form_post(self):
        signUp_form = SignUpForm(data={'email':'abcd@gmail.com', 'full_name': 'abcd'})
        self.assertFalse(signUp_form.is_valid())
        self.assertEqual(signUp_form.cleaned_data['email'],"abcd@gmail.com")
        self.assertEqual(signUp_form.cleaned_data['full_name'],"abcd")

    #ajax kritik
    def test_get_kritik(self):
        Kritik.objects.create(name='woi', message='xxxx')
        response = self.client.get('/kritikAjax/')
        self.assertEqual(404, response.status_code)

    def test_kritik_post_ajax(self):
        user = User.objects.create_user('hanifa', 'test@test.com', 'testpassword')
        self.client.force_login(user)
        response = self.client.post('/kritik/', {
                    'message' : "test"
                },
                HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        response = self.client.get('/kritik/')
        self.assertIn('test', response.content.decode('utf8'))


